#!/bin/sh -f
# Script to generate graph from latency_time data

GNUPLOT=/usr/bin/gnuplot

INFILES=''
INFILES_N=0

## functions

# bail:
#	...with success condition unless message is supplied
bail()
{
	STATUS=0
	if [ "$1" ] ; then
		printf "$0: $*\n\n" 1>&2
		STATUS=1
	fi

	echo "Usage: $0 [OPTIONS]" 1>&2
	echo 'Available options:' 1>&2
	printf "\t-h              show this help\n" 1>&2
	printf "\t-g              show horizontal 'grid' lines\n" 1>&2
	printf "\t-i INFILE       use given input file (data)\n" 1>&2
	printf "\t-o OUTFILE      use given output file (PNG image)\n" 1>&2
	printf "\t-m XMIN         crop X axis range on left\n" 1>&2
	printf "\t-M XMAX         crop X axis range on right\n" 1>&2
	printf "\t-x XRES         use given x resolution (image size)\n" 1>&2
	printf "\t-y YRES         use given y resolution (image size)\n" 1>&2
	exit ${STATUS}
}

# emit a gnuplot script
#	...with environment-configured behaviour
#	...suitable for passing to 'gnuplot -' or generating a .gp file
emit_gp()
{
	cat <<__EOF
#!${GNUPLOT}
set terminal png size ${PNGXRES},${PNGYRES}
set output '${OUTFILE}'

set title '{/:Bold Latency Variation Over Time}' font ',16'
set xlabel '{/:Bold Test Running Time}'
set ylabel '{/:Bold Latency Measurement Result}'

if ( '${YGRID}' eq 'y' ) set grid y

filenames='${INFILES}'

# Conversion functions

msec_to_sec(x) = ( x/1000000 )	# milliseconds to seconds
usec_to_msec(x) = ( x/1000 )	# microseconds to milliseconds


# Analyse data...

do for [w=1:words(filenames)] {
	# Assess the raw data to acquire initial timestamp value/s
	stats word(filenames,w) using 1 nooutput name sprintf('file%.0frawx_', w)

	# Compute statistics for the data we intend to present
	# (can bail if any requested range(s) are nonsensical)
	stats [${XMIN}:${XMAX}][*:*] word(filenames,w) \
		using (msec_to_sec( column(1)-value(sprintf('file%.0frawx_min',w)) )):(usec_to_msec(column(2))) \
		name sprintf('file%.0f_', w)
}


# Plot...

set format x "%.1fs"
set format y "%.2fms"
plot \
	[${XMIN}:${XMAX}][*:*] \
	for [w=1:words(filenames)] word(filenames,w) \
	using (msec_to_sec( column(1)-value(sprintf('file%.0frawx_min',w)) )):(usec_to_msec(column(2))) \
	with lines \
	title sprintf("%.0f points from '%s' (t_0_=%.0f; min %.0f{/Symbol m}s, max %.0f{/Symbol m}s)", value(sprintf('file%.0f_records',w)), word(filenames,w), value(sprintf('file%.0frawx_min',w)), value(sprintf('file%.0f_min_y',w)), value(sprintf('file%.0f_max_y',w)))
__EOF
}


## sanity checks

if [ ! -x ${GNUPLOT} ] ; then
	bail "${GNUPLOT} not installed"
fi

while getopts ":gi:o:m:M:x:y:h" OPT ; do
	case "${OPT}" in
	\?)
		bail "Unsupported option ${OPTARG}"
	;;
	:)
		bail "Missing argument to option ${OPTARG}"
	;;
	g)
		YGRID=y
	;;
	i)
		if [ ! -r ${OPTARG} ] ; then
			bail "Input file ${OPTARG} not found"
		fi
		INFILES="${INFILES:+${INFILES} }${OPTARG}"
		INFILES_N=$(( INFILES_N + 1))
	;;
	o)
		OUTFILE=${OPTARG}
	;;
	m)
		XMIN=${OPTARG}
	;;
	M)
		XMAX=${OPTARG}
	;;
	x)
		PNGXRES=${OPTARG}
	;;
	y)
		PNGYRES=${OPTARG}
	;;
	h)
		bail
	;;
	esac
done

shift $((OPTIND-1))
if [ "$1" ] ; then
	bail "Unexpected argument/s: $1"
fi


## ensure sane default behaviour

if [ ${INFILES_N} = 0 ] ; then
	INFILES=results.dat
	if [ ! -r ${INFILES} ] ; then
		bail "Input file ${INFILES} not found"
	fi
fi
if [ -z "${OUTFILE}" ] ; then
	if [ ${INFILES_N} -gt 1 ] ; then
		bail "Need '-o OUTFILE' due to multiple (${INFILES_N}x) input files"
	fi
	OUTFILE=${INFILES%.dat}'.png'
fi
if [ ! -w `dirname ${OUTFILE}` ] ; then
	bail "Output file '${OUTFILE}' not in writable directory"
fi

[ "${PNGXRES}" ] || PNGXRES=1600
[ "${PNGYRES}" ] || PNGYRES=800
[ "${XMIN}" ] || XMIN='*'
[ "${XMAX}" ] || XMAX='*'
[ "${YGRID}" ] || YGRID='n'


## run gnuplot

echo "Generating ${OUTFILE} from ${INFILES}..."

emit_gp | ${GNUPLOT} -
STATUS=$?

[ ${STATUS} = 0 ] || bail "gnuplot exited with status ${STATUS}" 1>&2
exit ${STATUS}
